import React, { useState } from 'react'
import Head from 'next/head'
import Link from 'next/link'

import styles from '../styles/Home.module.scss'

const Home: React.FC = () => {
  const [pageId, setPageId] = useState(0)

  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/assets/icons/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Estrutura com <a href="https://nextjs.org">Next.js!</a>
        </h1>

        <p className={styles.description}>
          Utilizando{' '}
          <span className={styles.techs}>
            TypeScript, Sass, ESLint e Prettier
          </span>
        </p>

        <div className={styles.grid}>
          <div className={styles.card}>
            <Link href={`/example/${pageId}`}>
              <a draggable="false">
                <h3>Exemplo {pageId} &rarr;</h3>

                <p>Altere o range para modificar o número do exemplo.</p>
              </a>
            </Link>
            <input
              type="range"
              min={0}
              max={50}
              value={pageId}
              onChange={e => setPageId(Number(e.target.value))}
            />
          </div>

          <Link href={'/image-example'}>
            <a draggable="false" className={styles.card}>
              <h3>Image do Next &rarr;</h3>

              <p>
                Otimização para o formato WebP (se suportado pelo navegador),
                lazy loading e mais.
              </p>
            </a>
          </Link>
        </div>
      </main>
    </div>
  )
}

export default Home
