import React from 'react'
import Head from 'next/head'
import Link from 'next/link'
import { useRouter } from 'next/router'

import styles from '../../styles/Example.module.scss'

const Example: React.FC = () => {
  const router = useRouter()
  const { id } = router.query

  return (
    <div className={styles.container}>
      <Head>
        <title>Exemplo {id}</title>
        <link rel="icon" href="/assets/icons/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>Exemplo {id}</h1>

        <Link href={'/'}>
          <a>&larr; Voltar</a>
        </Link>
      </main>
    </div>
  )
}

export default Example
