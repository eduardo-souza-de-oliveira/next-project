import React from 'react'
import Head from 'next/head'
import Link from 'next/link'
import Image from 'next/image'

import styles from '../styles/Example.module.scss'

const ImageExample: React.FC = () => {
  return (
    <div className={styles.container}>
      <Head>
        <title>Exemplo de imagem</title>
        <link rel="icon" href="/assets/icons/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>Componente Image do Next.js</h1>

        <div className={styles.gallery}>
          <Image src="/assets/images/bird.jpg" width={1920} height={1272} />
          <Image src="/assets/images/lion.jpg" width={1920} height={1277} />
          <Image src="/assets/images/snow.jpg" width={1920} height={1277} />
          <Image src="/assets/images/view.jpg" width={1920} height={1280} />
        </div>

        <Link href={'/'}>
          <a>&larr; Voltar</a>
        </Link>
      </main>
    </div>
  )
}

export default ImageExample
